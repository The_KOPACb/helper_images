# Helper Images
This repo contains some dockerfiles for helper images.

* kopacb/basic-tools:latest  
  Is alpine with `bash curl wget git ca-certificates openssl jq yq openssh-client openssl rsync zip unzip binutils upx inetutils-telnet` pre-installed

* kopacb/alt8-rpmdevtools:latest  
    Is alt p8 image with `rpm-build rpmdevtools apt-https curl wget ca-certificates nano which rsync` pre-installed

* kopacb/golang-upx  
    Is golang image with upx installed
    <details><summary>with tags:</summary><blockquote>

    latest  
    buster
    bullseye  
    alpine
    1.20-buster  
    1.20-alpine
    1.20-bullseye  
    1.19-buster  
    1.19-bullseye  
    1.18-buster  
    1.18-bullseye  
    1.20.2-stretch  
    1.19rc1-stretch  
    1.18-stretch  
    1.20-stretch  
    1.19-stretch  
  </blockquote></details>


* kopacb/docker-bash
  Is docker image with `bash yq openssh wget curl rsync` pre-installed  
    <details><summary>with tags:</summary><blockquote>

  latest  
  19  
  19.03  
  20  
  23  
  </blockquote></details>
